Path Access Inline Login Module
===============================

The Path Access Inline Login module is designed to prompt users to login before 
following a link which the Path Access module will block them from accessing. 
The module uses some simple javascript to rewrite the blocked URLs on the page 
and will display a login form using the Ajax Login/Regiser module.

Limitations
-----------
Path Access Inline Login only works for URLs blacklisted by the Path Access 
module. The whitelist configuration in the path access module is unsupported.

Dependencies
------------
Path Access Inline Login requires the following modules:

  * [Path Access](http://drupal.org/project/path_access) - requires 6.x-1.x-dev
  * [Ajax Login/Register](http://drupal.org/project/ajax_register)

Development
-----------
This module was developed by [Dave Hall Consulting](http://davehall.com.au).

More info
---------
To find out more, visit the [project page](http://drupal.org/node/1323564).
