Drupal.behaviors.pathAccessInlineLogin = function(context) {
  var patterns = Drupal.settings.pathAccessInlineLogin.patterns,
    patternCount = patterns.length,
    basePath = Drupal.settings.basePath.replace(/\//, '\\\/'),
    l = window.location,
    baseUrlRegExp = new RegExp('^' + l.protocol + '\\/\\/' + l.host + basePath);

  $('a:not(.inline-processed)', context).each(function() {
    var a = $(this),
      href = this.href.replace(baseUrlRegExp, ''),
      match;

    for (var i = 0; i < patternCount; i++) {
      match = href.match(new RegExp('^' + patterns[i] + '$'));
      if (match !== null) {
        a.attr('href', '?q=ajax_register/login&destination=' + encodeURIComponent(href));
        a.addClass('thickbox');
      }
    }
    a.addClass('inline-processed');
  });

  /*
    Hack to refresh the page so the user is recognised as authenticated after
    download a file such a doc or PDF.
  */
  $("#TB_window").unload(function() {window.location.reload()});
};

